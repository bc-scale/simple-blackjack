# Simple BlackJack

[![pipeline status](https://gitlab.com/artemdwo/simple-blackjack/badges/master/pipeline.svg)](https://gitlab.com/artemdwo/simple-blackjack/-/commits/master)

Simple BlackJack implementation with Python

## Milestone Project

* A simple text-based BlackJack game.
* The game has one player versus an automated dealer.
* The player can stand or hit.
* The player is able to pick their betting amount.
* The player's total money is being tracked
* There are alerts of wins, losses, or busts, etc...

## Requirements

* Python 3+

## Run tests
`python -m unittest test_*.py`

or 

`python3 -m unittest test_*.py`

## Run the game
`python main.py`

or 

`python3 main.py`