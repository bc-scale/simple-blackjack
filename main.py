from deck import Deck
from chips import Chips
from hand import Hand

"""
Blackjack card game
"""
playing = True


def take_a_bet(chips):
    while True:
        try:
            chips.bet = int(input('How many chips you\'d like to bet?: '))
        except:
            print('Please provide an integer value.')
        else:
            if chips.bet > chips.total:
                print(f'There is not enough chips. You have only {chips.total} ')
            else:
                break


def hit(deck, hand):
    hand.add_card(deck.deal())
    hand.adjust_for_ace()


def hit_or_stand(deck, hand):
    global playing

    while True:
        q = input('Hit(H|h) or Stand(S|s)?: ')

        if q[0].lower() == 'h':
            hit(deck, hand)
        elif q[0].lower() == 's':
            print('Player stands. Dealer\'s turn.')
            playing = False
        else:
            print('Please use H(h) for HIT or S(s) for STAND.')
            continue
        break


def show_some_cards(player, dealer):

    print('\nDealer\'s hand:')
    print('First card is hidden')
    print(dealer.cards[1])

    print('\nPlayer\'s hand:')
    for card in player.cards:
        print(card)


def show_all_cards(player, dealer):
    print('\nDealer\'s hand:')
    for card in dealer.cards:
        print(card)
    print(f'Dealer\'s hand value: {dealer.value}')

    print('\nPlayer\'s hand:')
    for card in player.cards:
        print(card)
    print(f'Player\'s hand value: {player.value}')


def player_busts(player, dealer, chips):
    print('Player BUSTED!')
    chips.lose_bet()


def player_wins(player, dealer, chips):
    print('Player WINs!')
    chips.win_bet()


def dealer_busts(player, dealer, chips):
    print('Player WINs! Dealer BUSTED!')
    chips.win_bet()


def dealer_wins(player, dealer, chips):
    print('Dealer WINs!')
    chips.lose_bet()


def push(player, dealer):
    print('Dealer and Player TIE! PUSH!')


def play_the_game():
    """
    Executable game logic
    """
    global playing

    while True:
        print('Welcome to simple Blackjack!')

        the_deck = Deck()
        the_deck.shuffle()

        players_hand = Hand()
        players_hand.add_card(the_deck.deal())
        players_hand.add_card(the_deck.deal())

        dealers_hand = Hand()
        dealers_hand.add_card(the_deck.deal())
        dealers_hand.add_card(the_deck.deal())

        players_chips = Chips()
        take_a_bet(players_chips)
        show_some_cards(players_hand, dealers_hand)

        while playing:
            hit_or_stand(the_deck,players_hand)
            show_some_cards(players_hand, dealers_hand)

            if players_hand.value > 21:
                player_busts(players_hand, dealers_hand, players_chips)
                break

        if players_hand.value <= 21:

            while dealers_hand.value < 17:
                hit(the_deck, dealers_hand)

            show_all_cards(players_hand, dealers_hand)

            if dealers_hand.value > 21:
                dealer_busts(players_hand, dealers_hand, players_chips)
            elif dealers_hand.value > players_hand.value:
                dealer_wins(players_hand, dealers_hand, players_chips)
            elif dealers_hand.value < players_hand.value:
                player_wins(players_hand, dealers_hand, players_chips)
            else:
                push(players_hand, dealers_hand)

        print(f'\n Player total chips are at: {players_chips.total}')

        new_game = input('Would you like to play another hand? (Y/N): ')
        if new_game[0].lower() == 'y':
            playing = True
            continue
        else:
            print('\nSee you next time!')
            break


if __name__ == '__main__':
    play_the_game()
