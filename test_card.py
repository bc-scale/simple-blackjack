import unittest
from card import Card


class TestCardClass(unittest.TestCase):

    def setUp(self):
        suit = 'Clubs'
        rank = 'Three'
        self.new_card = Card(suit, rank)

    def test_card_str(self):
        self.assertEqual('Three of Clubs', str(self.new_card))

    def test_card_suit(self):
        self.assertEqual('Clubs', self.new_card.suit)

    def test_card_rank(self):
        self.assertEqual('Three', self.new_card.rank)

    def test_card_init_value(self):
        self.assertEqual(3, self.new_card.value)
